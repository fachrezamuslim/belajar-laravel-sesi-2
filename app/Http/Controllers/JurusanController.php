<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jurusan;

class JurusanController extends Controller
{
    function daftar(){
        // mengelola data dari database
        // menentukan view
        // return "Menampilkan daftar jurusan";
        return view('jurusan/index');
    }

    function create(){
        return view('jurusan/create');
    }

    function add(Request $request){
        // dd($request->all());
        
        // Cara 1
        // $create = new Jurusan;
        // $create->nama_jurusan = $request->nama_jurusan;
        // $create->kode_jurusan = $request->kode_jurusan;
        // $create->deskripsi = $request->deskripsi;
        // $create->save();

        // Cara 2
        $create = Jurusan::create([
            'nama_jurusan'      => $request->nama_jurusan,
            'kode_jurusan'      => $request->kode_jurusan,
            'deskripsi'         => $request->deskripsi,
        ]);

        return redirect()->back()->with('pesan', 'Berhasil menambah jurusan');
    }

    function find($id){
        return "Mencari jurusan dengan ID : " . $id;
    }

    function edit($id){
        return "Menampilan haaman edit untuk jurusan dengan ID : " . $id;
    }

    function delete($id){
        return "Menghapus jurusan dengan ID : " . $id;
    }

    function view($jurusan_id){
        // dd($jurusan_id);
        $data['jurusan'] = Jurusan::where('id', $jurusan_id)->first();
        return view('jurusan.view', $data);
    }
}
