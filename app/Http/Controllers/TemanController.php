<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teman;

class TemanController extends Controller
{
    function create(){
        return view('teman.create'); // teman/create
    }

    function store(Request $request){
        // dd($request->all());  

        $dataToCreate = [
            'nama'      => $request->nama,
            'deskripsi' => $request->deskripsi
        ];

        $create = Teman::create($dataToCreate);

        return redirect('/teman/list');
    }

    function list(){
        $data['teman'] = Teman::all();
        $data['tanggal'] = "Hari ini adalah hari minggu";

        return view('teman.list', $data);
    }

    function edit($id){
        // dd($id);
        $data['teman'] = Teman::where('id', $id)->first();

        return view('teman.edit', $data);
    }

    function update (Request $request) {
        // dd($request->all());

        $update = Teman::where('id', $request->id)->first();
        $update->nama = $request->nama;
        $update->deskripsi = $request->deskripsi;
        $update->save();

        return redirect('teman/list');
    }

    function delete ($id){
        $delete = Teman::where('id', $id)->first();
        $delete->delete();

        return redirect('teman/list');
    }
}
