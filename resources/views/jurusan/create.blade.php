@extends('layout.app') 
@section('title', 'Tambah Jurusan') 
@section('content')
<h1>Tambah Jurusan</h1>
<div class="row">
    <div class="col-md-12">
        <form method="post" action="{{ url('jurusan/add') }}">
            @csrf
            <div class="form-group">
                <label for="">Nama Jurusan</label>
                <input name="nama_jurusan" type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Kode Jurusan</label>
                <input name="kode_jurusan" type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Deskripsi</label>
                <textarea name="deskripsi" class="form-control"></textarea>
            </div>
            <button class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection