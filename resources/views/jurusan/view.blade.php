@extends('layout.app') 
@section('title', 'View Jurusan') 
@section('content')
<h1>View Jurusan</h1>
<div class="row">
    <div class="col-md-12">
        <form method="post" action="{{ url('jurusan/add') }}">
            <div class="form-group">
                <label for="">Nama Jurusan</label>
                <input disabled value="{{ $jurusan->nama_jurusan }}" type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Kode Jurusan</label>
                <input disabled value="{{ $jurusan->kode_jurusan }}" type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Deskripsi</label>
                <textarea disabled name="deskripsi" class="form-control">{{ $jurusan->deskripsi }}</textarea>
            </div>
        </form>
    </div>
</div>
@endsection