@extends('layout.layout')
@section('content')
    <h1>Tambah Teman</h1>
    <form method="post" action="{{ url('teman/store') }}">
        @csrf
        <div>
            <label for="">Nama</label> <br />
            <input type="text" name="nama">
        </div>

        <br />

        <div>
            <label for="">Deskripsi</label> <br />
            <textarea name="deskripsi"></textarea>
        </div>

        <button>Tambah</button>
    </form>
@endsection