<p>
    <a href="{{ url('/teman/create') }}">Tambah Teman</a>
</p>

<table width="100%" border="1">
    <tr>
        <th>Nama</th>
        <th>Deskripsi</th>
        <th></th>
    </tr>
    @foreach ($teman as $data)
        <tr>
            <td>{{ $data->nama }}</td>
            <td>{{ $data->deskripsi }}</td>
            <td>
                <a target="_blank" href="{{ url('teman/edit/' . $data->id) }}">edit</a>
                <a href="{{ url('teman/delete/' . $data->id) }}">delete</a>
            </td>
        </tr>
    @endforeach
</table> 