<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'user'], function () {
    Route::get('/', function(){
        return "Halaman User";
    });

    Route::post('add', function(){
        return "Halaman tambah user";
    });

    Route::get('{id}/get', function($id){
        return $id;
    });
});


// Jurusan
Route::group(['prefix' => 'jurusan'], function () {
    Route::get('/', 'JurusanController@daftar');
    Route::get('/create', 'JurusanController@create');
    Route::get('{jurusan_id}/view', 'JurusanController@view');
    // Route::get('{id}/edit', 'JurusanController@edit');
    // Route::get('{id}/delete', 'JurusanController@delete');

    Route::post('add', 'JurusanController@add');
    Route::post('update', 'JurusanController@update');
});

Route::group(['prefix' => 'pengarang'], function () {    
    Route::get('/', 'PengarangController@index');
    Route::get('{id}/find', 'PengarangController@find');
});

Route::get('teman/create', 'TemanController@create');
Route::post('teman/store', 'TemanController@store');
Route::get('teman/list', 'TemanController@list');

Route::get('teman/edit/{id}', 'TemanController@edit');
Route::post('teman/update', 'TemanController@update');

Route::get('teman/delete/{id}', 'TemanController@delete');